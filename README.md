# simxu - simxu lo ka xamgu


## Introduction
simxu is an Open Source, multiplatform game based on the [Edencraft Engine](http://github.com/Isilin/EdenCraft).


## Compiling simxu
This project use [Premake5](https://premake.github.io/download.html) to generate the project files. You need don't need to download Premake5, as it is provided in the build directory. To build the game, use the premake script, according to your system, then compile it.

## Using simxu
Tutorials and documentation is not available yet. Work in progress.

## Contribution to simxu
Bugs can be reported on the Github issue tracker here: [![GitHub issues](https://img.shields.io/github/issues/Isilin/simxu.svg)](https://github.com/Isilin/simxu/issues)

## Authors
* IsilinBN
* Alcyone - http://alcyone.toile-libre.org/

## Copyright and Licensing
simxu is delivered under the [GNU-GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html).

simxu is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## F.A.Q.

### **Q:** Why "simxu" does not begin with a capital letter ? 
> **A:** Lojban doesn’t use lower-case (small) letters and upper-case (capital) letters in the same way that English does; sentences do not begin with an upper-case letter, nor do names. However, upper-case letters are used in Lojban to mark irregular stress within names. - **The Complete Lojban Language by John Woldemar Cowan**